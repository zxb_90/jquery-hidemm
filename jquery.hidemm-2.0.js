var Hidemm = function(el){
	this.el = el;
	this.val = "";
	this.selectionStart = -1;
	this.selectionEnd = -1;
	this.inputSelectionEnd = -1;
	this.init = function(){
		var _this = this;
		$(this.el).on('input',function(){
			_this.inputSelectionEnd = $(this)[0].selectionEnd;
			
			var value = $(this).val();
			
			if(value){
				if(_this.selectionStart == -1){
					if(value.length > _this.val.length){
						_this.selectionStart = _this.inputSelectionEnd - Math.abs(value.length-_this.val.length);
						// console.log(_this.selectionStart);
						_this.selectionEnd = _this.selectionStart + _this.inputSelectionEnd - _this.selectionStart;
						// console.log(_this.selectionEnd);
						var prefix = _this.val.substring(0,_this.selectionStart);
						// console.log("prefix="+prefix);
						var middle = value.substring(_this.selectionStart,_this.inputSelectionEnd);
						// console.log("middle="+middle);
						var suffix = _this.val.substring(_this.selectionStart,_this.val.length);
						// console.log("suffix="+suffix);
						_this.val = prefix + middle + suffix;
					}else if(value.length < _this.val.length){
						// _this.val = _this.val.substring(0,value.length);
						_this.selectionStart = _this.inputSelectionEnd + Math.abs(value.length-_this.val.length);
						// console.log(_this.selectionStart);
						var prefix = _this.val.substring(0,_this.inputSelectionEnd);
						// console.log("prefix="+prefix);
						var suffix = _this.val.substring(_this.selectionStart,_this.val.length);
						// console.log("suffix="+suffix);
						_this.val = prefix + suffix;
					}
				}else{
					var prefix = _this.val.substring(0,_this.selectionStart);
					// console.log("prefix="+prefix);
					var middle = value.substring(_this.selectionStart,_this.inputSelectionEnd);
					// console.log("middle="+middle);
					var suffix = _this.val.substring(_this.selectionEnd,_this.val.length);
					// console.log("suffix="+suffix);
					_this.val = prefix + middle + suffix;
				}
				$(_this.el).val(value.replace(/./g,'*'));
				//console.log(_this.val);
			}else{
				_this.val = "";
				$(_this.el).val("");
			}
			_this.selectionStart = -1;
			_this.selectionEnd = -1;
			_this.inputSelectionEnd = -1;
		});
		$(this.el).on('select',function(){
			_this.selectionStart = $(this)[0].selectionStart;
			_this.selectionEnd = $(this)[0].selectionEnd;
		})
	}
	this.init();
}

Hidemm.prototype.getVal = function(){
	return this.val;
}
